import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, ToastController, LoadingController, ItemSliding, AlertController } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';



@Component({
  selector: 'page-edit-signal',
  templateUrl: 'edit-signal.html',
})



export class EditSignalPage {
  
  
  
  deviceRadio: string = "";
  sinal: string;
  deviceName: string = "";
  SignalName: string = "";
  segmento: any;
  ligado: boolean = false;
  listaSinal: boolean = false;



  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,   
    private alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    public data: DataProvider) {

  }


  ionViewDidEnter() {
    console.log('ionViewDidEnter EditSignalPage');
    console.log(this.data.devicesListMap);
    
    this.segmento = "Dispositivo";
  }


  removeItem(signal, itemSliding: ItemSliding) {
    console.log(signal);
    let alert = this.alertCtrl.create({
      title: 'Atenção',
      message: 'Deseja excluir o sinal: ' + signal.name[1],
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'Sim',
          handler: () => {
            console.log('Sim clicked');
            this.data.deleteSignal(signal.id).then(data => {
              if (data) {                               

                let toast = this.toastCtrl.create({
                  message: "Sinal excluido com sucesso!",
                  duration: 1000,
                  position: 'top',
                  cssClass: "toast-success",
                  closeButtonText :'Fechar',
                  showCloseButton:false
                });
              
                toast.onDidDismiss(() => {
                  //this.dismiss();
                });
                this.data.updateDeviceListMap();
                this.deviceRadio="";
                this.listaSinal=false;              
                toast.present();
              }else{
                let toast = this.toastCtrl.create({
                  message: "Problema de Comunicação",
                  duration: 1000,
                  position: 'top',
                  cssClass: "toast-error",
                  closeButtonText :'Fechar',
                  showCloseButton:false
                });
              
                toast.onDidDismiss(() => {
                  //console.log('Dismissed toast');
                });
                
                toast.present();
              }
            
            itemSliding.close();
          });
        }},
        {
          text: 'Não',
          role: 'cancel',
          handler: () => {
            console.log('Não clicked');
            itemSliding.close();
          }
        }
      ]
    });
    alert.present();
  }

  editItem(device,signal, itemSliding: ItemSliding){
  
      let alert = this.alertCtrl.create({
        title: 'Editar',
        message: 'Digite o novo nome para: ' +  signal.name[1],
        inputs: [
          {
            name: 'Nome',
            value: signal.name[1]
          }
          
        ],
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
            handler: data => {
              console.log('Cancel clicked');
              itemSliding.close();
            }
          },
          {
            text: 'Salvar',
            handler: data => {
              this.data.renameSignal(signal.id,device+"/"+data.Nome).then(data => {
                if (data) {
                  let toast = this.toastCtrl.create({
                    message: "Sinal editado com sucesso!",
                    duration: 1000,
                    position: 'top',
                    cssClass: "toast-success",
                    closeButtonText :'Fechar',
                    showCloseButton:false
                  });
                
                  toast.onDidDismiss(() => {
                    //this.dismiss();
                  }); 
                  this.data.updateDeviceListMap();
                  this.deviceRadio="";
                  this.listaSinal=false;             
                  toast.present();
                }else{
                  let toast = this.toastCtrl.create({
                    message: "Problema de Comunicação",
                    duration: 1000,
                    position: 'top',
                    cssClass: "toast-error",
                    closeButtonText :'Fechar',
                    showCloseButton:false
                  });
                
                  toast.onDidDismiss(() => {
                    //console.log('Dismissed toast');
                  });                
                  toast.present();
                }                           
              itemSliding.close();
            });
            }
          }
        ]
      });
      alert.present();
  
  }

  removeDevice(name, itemSliding: ItemSliding) {
    console.log(name);
    let alert = this.alertCtrl.create({
      title: 'Atenção',
      message: 'Deseja excluir o Dispositivo: ' + name+' e todos os seus sinais?',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'Sim',
          handler: () => {
            console.log('Sim clicked'); 

            this.data.deleteDevice(name).then(data => {
                if (data) {
                  let toast = this.toastCtrl.create({
                    message: "Dispositivo exclido com sucesso!",
                    duration: 1000,
                    position: 'top',
                    cssClass: "toast-success",
                    closeButtonText :'Fechar',
                    showCloseButton:false
                  });
                
                  toast.onDidDismiss(() => {
                    //this.dismiss();
                  });
                  this.data.updateDeviceListMap();              
                  toast.present();
                }else{
                  let toast = this.toastCtrl.create({
                    message: "Problema de Comunicação",
                    duration: 1000,
                    position: 'top',
                    cssClass: "toast-error",
                    closeButtonText :'Fechar',
                    showCloseButton:false
                  });
                
                  toast.onDidDismiss(() => {
                    //console.log('Dismissed toast');
                  });                
                  toast.present();
                }                           
              itemSliding.close();
            });
          }
        },
        {
          text: 'Não',
          role: 'cancel',
          handler: () => {
            console.log('Não clicked');
            itemSliding.close();
          }
        }
      ]
    });
    alert.present();
  }

  editDevice(device, itemSliding: ItemSliding) {
    let alert = this.alertCtrl.create({
      title: 'Editar',
      inputs: [
        {
          name: 'Nome',
          value: device
        }
        
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
            itemSliding.close();
          }
        },
        {
          text: 'Salvar',
          handler: data => {
            this.data.renameDevice(device,data.Nome).then(data => {
              if (data) {
                let toast = this.toastCtrl.create({
                  message: "Dispositivo exclido com sucesso!",
                  duration: 1000,
                  position: 'top',
                  cssClass: "toast-success",
                  closeButtonText :'Fechar',
                  showCloseButton:false
                });
              
                toast.onDidDismiss(() => {
                  //this.dismiss();
                });
                this.data.updateDeviceListMap();              
                toast.present();
              }else{
                let toast = this.toastCtrl.create({
                  message: "Problema de Comunicação",
                  duration: 1000,
                  position: 'top',
                  cssClass: "toast-error",
                  closeButtonText :'Fechar',
                  showCloseButton:false
                });
              
                toast.onDidDismiss(() => {
                  //console.log('Dismissed toast');
                });                
                toast.present();
              }                           
            itemSliding.close();
          });            
           
          }
        }
      ]
    });
    alert.present();
  }



  selecionarSegmento() {
    console.log("segmento: ", this.segmento)
    if(this.segmento=='Sinal'){
      this.ligado = true;
    }else{
      this.ligado = false;
    }    
  }



  getKeys(map) {
    return Array.from(map.keys());
  }



  radio_select(value) {
    console.log(value)
    this.listaSinal = true;
  }

  
}
