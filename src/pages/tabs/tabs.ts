import { Component } from '@angular/core';
import { HomePage } from '../home/home';

import { Platform } from 'ionic-angular/platform/platform';
import { AddSignalPage } from '../add-signal/add-signal';
import { EditSignalPage } from '../edit-signal/edit-signal';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;  
  tab2Root = AddSignalPage;
  tab3Root = EditSignalPage;
  

  constructor(private platform: Platform) {
    
  }
  
  
  
}
