
import { Injectable } from '@angular/core';

import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';


import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/catch';


@Injectable()
export class DataProvider {
  public devicesListMap: Map<string, any[]> = new Map<string, any[]>();
  public hostIP: string;
  public row: number;
  public col: number;
  constructor(private storage: Storage,
    private http: Http
    ) {
    console.log('Hello DataProvider Provider');
  }




  loadIP(): Promise<any> {

    return new Promise(resolve => {
      this.storage.get('ipmdns').then((data) => {
        console.log("value DATA LOAD :", data)
        this.hostIP = data;
        resolve(data);
      });
    })

  }

  loadDeviceListMap(): Promise<Boolean> {

    return this.storage.length().then(result => {
      //console.log('storage.length :'+result);
      if (result > 0) {
        this.devicesListMap.clear();
        var self = this;
        this.storage.get('data').then(result => {
          // Or to get a key/value pair
          if (result != null) {
            this.storage.get('data').then((val) => {
              val.forEach(function (value) {

                if (self.devicesListMap.size > 0) {
                  if (self.devicesListMap.has(value.name[0])) {
                    self.devicesListMap.get(value.name[0]).push(value);
                  }
                  else {
                    let myarray: String[] = [];
                    myarray.push(value);
                    self.devicesListMap.set(value.name[0], myarray);
                  }
                }
                else {
                  let myarray: String[] = [];
                  myarray.push(value);
                  self.devicesListMap.set(value.name[0], myarray);
                }
              });
            });

            this.storage.get('row').then((val) => {
              self.row = val;
            });

            this.storage.get('col').then((val) => {
              self.col = val;
            });

            console.log("read data componente" + this.devicesListMap);
            self.devicesListMap = self.devicesListMap;
            return true;
          } else {

            this.devicesListMap.clear();

            this.http.get('http://' + this.hostIP + '/info').timeout(1500).map(res => res.json()).subscribe(data => {

              var self = this;
              var row = 1;
              var col = 0;
              data.signals.forEach(function (value) {
                value.name = value.name.split('/');
                if (value.row > row) {
                  row = value.row;
                  col = value.column;

                } else if (value.row = row) {
                  if (value.column >= col) {
                    col = value.column;
                  }
                } else {

                }

                if (self.devicesListMap.size > 0) {
                  if (self.devicesListMap.has(value.name[0])) {
                    self.devicesListMap.get(value.name[0]).push(value);
                  }
                  else {
                    let myarray: String[] = [];
                    myarray.push(value);
                    self.devicesListMap.set(value.name[0], myarray);
                  }
                }
                else {
                  let myarray: String[] = [];
                  myarray.push(value);
                  self.devicesListMap.set(value.name[0], myarray);
                }
              });
              // set a key/value
              this.storage.set('data', data.signals);
              this.storage.set('row', row);
              this.row = row;
              this.storage.set('col', col);
              this.col = col;

              this.devicesListMap = this.devicesListMap;


              console.log("update data componente" + this.devicesListMap);
              return true;
            },
              err => {
                console.log("error get info");
                return false;
              });
          }
        });
        return true;
      }
    });



  }


  updateDeviceListMap(): Promise<Boolean> {
    return new Promise((resolve, reject) => {
      this.devicesListMap.clear();
      this.http.get('http://' + this.hostIP + '/info').timeout(1500).map(res => res.json()).subscribe(data => {

        var self = this;
        var row = 1;
        var col = 0;
        data.signals.forEach(function (value) {
          value.name = value.name.split('/');
          if (value.row > row) {
            row = value.row;
            col = value.column;

          } else if (value.row = row) {
            if (value.column >= col) {
              col = value.column;
            }
          } else {

          }

          if (self.devicesListMap.size > 0) {
            if (self.devicesListMap.has(value.name[0])) {
              self.devicesListMap.get(value.name[0]).push(value);
            }
            else {
              let myarray: String[] = [];
              myarray.push(value);
              self.devicesListMap.set(value.name[0], myarray);
            }
          }
          else {
            let myarray: String[] = [];
            myarray.push(value);
            self.devicesListMap.set(value.name[0], myarray);
          }
        });
        // set a key/value
        this.storage.set('data', data.signals);
        this.storage.set('row', row);
        this.row = row;
        this.storage.set('col', col);
        this.col = col;

        this.devicesListMap = this.devicesListMap;


        console.log("update data componente" + this.devicesListMap);
        resolve(true);
      },
        err => {
          console.log("error get info");
          resolve(false);
        });
    });


  }



  
  sendSignal(id): Promise<Boolean> {
    return new Promise((resolve, reject) => {
      this.http.get('http://' + this.hostIP + '/signals/send?id=' + id).timeout(1500).subscribe(data => {
        //console.log(data);

        resolve(true);

      },
        err => {
          console.log("error get info");
          resolve(false);
        });
    });


  }

  recordSignal(name, row, col): Promise<Boolean> {
    return new Promise((resolve, reject) => {

      this.http.get('http://' + this.hostIP + '/signals/record?row=' + row + '&column=' + col + '&name=' + name).timeout(5000).subscribe(data => {
        //console.log(data);
        resolve(true);
      },
        err => {
          resolve(false);
        });


    })
  }


  renameSignal(id, name): Promise<Boolean> {
    return new Promise((resolve, reject) => {

      this.http.get('http://' + this.hostIP + '/signals/rename?id=' + id + '&name=' + name).timeout(1500).subscribe(data => {
        //console.log(data);

        resolve(true);

      },
        err => {
          resolve(false);

        })
    });

  }

  renameDevice(oldName, newName) {
    return new Promise((resolve, reject) => {
    var self = this;
    this.devicesListMap.get(oldName).forEach(function (value) {
      console.log(value.id);
      self.http.get('http://' + self.hostIP + '/signals/rename?id=' + value.id + '&name=' + newName + "/" + value.name[1]).timeout(1500).subscribe(data => {
        //console.log(data);
        
        resolve(true);

      },
        err => {
          resolve(false);
        })
      });
    });

  }

  deleteSignal(id) {
    return new Promise((resolve, reject) => {
    this.http.get('http://' + this.hostIP + '/signals/clear?id=' + id).timeout(1500).subscribe(data => {
      //console.log(data);
      
      resolve(true);
    },
      err => {
        
        resolve(false);
      });
    })

  }



  deleteDevice(name) {
    return new Promise((resolve, reject) => {
    var self = this;
    this.devicesListMap.get(name).forEach(function (value) {
      console.log(value.id);
      self.http.get('http://' + self.hostIP + '/signals/clear?id=' + value.id).timeout(1500).subscribe(data => {
        //console.log(data);
        
        resolve(false);
      },
        err => {
          resolve(false);
        });

    });

  })

  }






}

