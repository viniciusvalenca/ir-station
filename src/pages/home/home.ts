import { Component } from '@angular/core';
import { NavController, LoadingController, ToastController, AlertController, ModalController, App } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { FirstrunPage } from '../firstrun/firstrun';
import { DataProvider } from '../../providers/data/data';



@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})


export class HomePage {

  constructor(public navCtrl: NavController,
    public loadingCtrl: LoadingController,    
    private toastCtrl: ToastController,
    private storage: Storage,
    private alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public data: DataProvider,
    private app: App) {
  }


  doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    this.data.updateDeviceListMap().then(data => {
      if (data) {
        refresher.complete();
      } else {
        refresher.complete();
        let alert = this.alertCtrl.create({
          title: 'Erro',
          message: 'Problema de comunicação',
          enableBackdropDismiss: false,
          buttons: [
            {
              text: 'Reconfigurar',
              handler: () => {
                console.log('Reconfigurar clicked');
                this.storage.clear();
                this.app.getRootNav().setRoot(FirstrunPage);
              }
            },
            {
              text: 'Tentar novamente',
              handler: () => {
                console.log('Tentar novamente clicked');
                this.updateSignals();
              }
            },
            {
              text: 'Fechar',
              role: 'cancel',
              handler: () => {
                console.log('Fechar clicked');
                this.data.loadDeviceListMap();                
              }
            }
          ]
        });
        alert.present();
      }
    });
  }




  updateSignals() {
    let loading = this.loadingCtrl.create({
      content: 'Atualizando...'
    });
    loading.present();
    this.data.updateDeviceListMap().then(data => {
      if (data) {
        loading.dismiss();
      } else {
        loading.dismiss();
        let alert = this.alertCtrl.create({
          title: 'Erro',
          message: 'Problema de comunicação',
          enableBackdropDismiss: false,
          buttons: [
            {
              text: 'Reconfigurar',
              handler: () => {
                console.log('Reconfigurar clicked');
                this.storage.clear();
                this.app.getRootNav().setRoot(FirstrunPage);
              }
            },
            {
              text: 'Tentar novamente',
              handler: () => {
                console.log('Tentar novamente clicked');
                this.updateSignals();
              }
            },
            {
              text: 'Fechar',
              role: 'cancel',
              handler: () => {
                console.log('Fechar clicked');
                this.data.loadDeviceListMap();                 
              }
            }
          ]
        });
        alert.present();
      }
    });
  }

  
  ionViewDidEnter() {
    console.log('ionViewDidEnter HomePage');
    console.log(this.data.devicesListMap);  
  }



  sendSignal(id) {
    this.data.sendSignal(id).then(data => {
      if (data) {
        let toast = this.toastCtrl.create({
          message: "Sinal enviado",
          duration: 1000,
          position: 'top',
          cssClass: "toast-success",
          closeButtonText: 'Fechar',
          showCloseButton: false
        });
  
        toast.onDidDismiss(() => {
          //console.log('Dismissed toast');
        });
  
        toast.present();
      } else {
        let toast = this.toastCtrl.create({
          message: "Problema de Comunicação",
          duration: 1000,
          position: 'top',
          cssClass: "toast-error",
          closeButtonText: 'Fechar',
          showCloseButton: false
        });

        toast.onDidDismiss(() => {
          //console.log('Dismissed toast');
        });

        toast.present();
      }
    });
  }

  getKeys(map) {
    return Array.from(map.keys());
  }


}





