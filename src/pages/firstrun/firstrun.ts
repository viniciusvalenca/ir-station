import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { Zeroconf } from '@ionic-native/zeroconf';
import { Storage } from '@ionic/storage';
import { HomePage } from '../home/home';
import { TabsPage } from '../tabs/tabs';
import { DataProvider } from '../../providers/data/data';
/**
 * Generated class for the FirtrunPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-firstrun',
  templateUrl: 'firstrun.html',
})
export class FirstrunPage {
  servicoMDNS: any[ ]=[];

  

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private storage: Storage, private zeroconf: Zeroconf,
    public alertCtrlr: AlertController,
    public loader: LoadingController,
    public data:DataProvider) {
      
      this.watchMDNS();
     
    }
    
  


  watchMDNS(){
    this.servicoMDNS=[];
    this.zeroconf.close();
    let loading = this.loader.create({
      content: 'Buscando...'
    });
    // watch for services of a specified type
     this.zeroconf.watch('_arduino._tcp.', 'local.').subscribe(result => {
      //console.log('result', result);
      if (result.action == 'added') {
        console.log('service added', result.service);
      } else {
        console.log('service removed', result.service);
              if (String(result.service.ipv4Addresses) != '0.0.0.0') {
                this.servicoMDNS.push(result.service);
              }
      }
    });
    loading.present();
    
    setTimeout(() => {
      loading.dismiss();
      if(this.servicoMDNS.length==0){
        let alert = this.alertCtrlr.create({
          title: 'Erro',
          message: 'Nenhum dispositivo encontrado. Caso nao tenha configurado na sua rede, conecte na wifi IR-Station e abra o aplicativo novamente.',
          enableBackdropDismiss: false,
          buttons: [  
            {
              text: 'Fechar',
              role: 'cancel',
              handler: () => {
                console.log('Fechar clicked');
                
              }
            }
          ]
        });
        alert.present();

      }
    }, 1000);  
    console.log('servicoMDNS', this.servicoMDNS);
  }


  selecionarLuminaria(nmdns, ipmdns) {    
    let confirm = this.alertCtrlr.create({
      title: 'Confirmação.',
      message: 'Deseja realmente conectar-se ao IR-Station: ' + nmdns + ' ?',
      buttons: [
        {
          text: 'NÃO',
          handler: () => {            
            console.log('Recusa conexão');
          }
        },
        {
          text: 'SIM',
          handler: () => {
            this.getMDNS(nmdns, ipmdns);
            console.log('Confirma conexão');
          }
        }
      ]
    });
    confirm.present();
  }

  getMDNS(nmdns, ipmdns) {
    this.zeroconf.close();
    console.log("nmdns: ", nmdns);
    console.log("ipmdns: ", ipmdns);

    this.storage.set('nmdns', nmdns);
    this.data.hostIP=ipmdns;
    this.storage.set('ipmdns', ipmdns);
    this.data.loadDeviceListMap().then(data=>{
      this.navCtrl.setRoot(TabsPage);
    })
    
  }

}
