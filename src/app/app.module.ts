import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { FirstrunPage } from '../pages/firstrun/firstrun';
import { WizardPage } from '../pages/wizard/wizard';
import { AddSignalPage } from '../pages/add-signal/add-signal';

import { HttpModule } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';
import { Zeroconf } from '@ionic-native/zeroconf';
import { TabsPage } from '../pages/tabs/tabs';
import { DataProvider } from '../providers/data/data';
import { EditSignalPage } from '../pages/edit-signal/edit-signal';




@NgModule({
  declarations: [
    MyApp,
    HomePage,
    FirstrunPage,
    WizardPage,
    AddSignalPage,
    EditSignalPage,
    TabsPage
  ],
  imports: [
    BrowserModule,
    HttpModule,    
    IonicModule.forRoot(MyApp, {
      autocomplete: 'on'
    }),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    FirstrunPage,
    WizardPage,
    AddSignalPage,
    EditSignalPage,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Zeroconf,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    DataProvider
  ]
})
export class AppModule {}
