import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Slides, Content, LoadingController } from 'ionic-angular';
import { Http } from '@angular/http';
import { FirstrunPage } from '../firstrun/firstrun';


@Component({
  selector: 'page-wizard',
  templateUrl: 'wizard.html',
})
export class WizardPage {

  
  @ViewChild(Slides) slides: Slides;
  @ViewChild(Content) content: Content;

  wifiList: any[ ];
  wifiSSID:string ="" ;
  wifiPASS:string ="";
  hostname:string ="";
  stealth:boolean = false;
  public type = 'password';
  public showPass = false;
  public refresherEnabled = true;
  currentIndex: number;
  objectIndex: number;

  constructor(public navCtrl: NavController, private http: Http, 
    public loadingCtrl: LoadingController, public navParams: NavParams,
    ) {

      this.loadWifi();
    
  }

  loadWifi(){
    let loading = this.loadingCtrl.create({
      content: 'Carregando...'
    });
  
    loading.present();
    this.http.get('http://192.168.4.1/wifi/list').timeout(3000).map(res => res.json()).subscribe(data => {
      console.log(data);
      this.wifiList=data;
      loading.dismiss(); 
  },
  err => {
    console.log("error");
    loading.dismiss(); 
  });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ServicePage');
    this.slides.lockSwipes(true);    
  }
 

  goToSlide(slideNo) {
    this.slides.lockSwipes(false);
    this.content.scrollToTop();
    this.slides.slideTo(slideNo);
    this.slides.lockSwipes(true);
  }

  wifiClick(wifi){
    console.log('Clicked: '+wifi);
    if(wifi!=undefined){
    this.wifiSSID=wifi;
  }else
  this.wifiSSID=""
    this.refresherEnabled=false;
    this.goToSlide(1);
    
  }

  wifiStealthClick(){
    console.log('Clicked: Stealth');
    this.stealth=true;
    this.goToSlide(1);
    
  }

  saveWifi(){
    
    console.log('Save:');

    this.http.get('http://192.168.4.1/mode/station', 
    { params: {'ssid':this.wifiSSID,'password':this.wifiPASS,'stealth':this.stealth, 'hostname':this.hostname }}).timeout(1000).subscribe(data => {
      console.log(data);
      this.goToSlide(2);
  },
  err => {    
  });

  }

  disableSave() {
    if (this.wifiPASS=="" || this.wifiPASS=="" || this.hostname=="") return true ;
             return false ;
}

showBack() {
  if (this.slides.getActiveIndex()>0) return true ;
           return false ;
}

doRefresh(refresher) {
  this.http.get('http://192.168.4.1/wifi/list').timeout(3000).map(res => res.json()).subscribe(data => {
    console.log(data);
    this.wifiList=data;
    refresher.complete(); 
},
err => {
  console.log("error");
  refresher.complete();
});
}


 
  showPassword() {
    this.showPass = !this.showPass;
 
    if(this.showPass){
      this.type = 'text';
    } else {
      this.type = 'password';
    }
  }


  confirmWifi(){   
      let loading = this.loadingCtrl.create({
        content: 'Please wait...'
      });
    
      loading.present();
      

      setTimeout(() => {
        loading.dismiss();
        this.http.get('http://192.168.4.1/wifi/confirm').timeout(1000).subscribe(data => {
        console.log(data);
        if(data['_body'] ==='false')   {
          //alert("Problema na configuração");
          this.goToSlide(0);
          this.loadWifi();
        }else{
          //alert("true");
          this.navCtrl.setRoot(FirstrunPage);
        }
    },
    err => {    
    });
        
      }, 10000);

      
  }

  slidesBack() {
    console.log(this.slides.getActiveIndex())
      this.currentIndex = this.slides.getActiveIndex();
      if(this.currentIndex-1==0){
        this.refresherEnabled=true;
      }
      this.goToSlide(this.currentIndex-1);
 }

 

}
