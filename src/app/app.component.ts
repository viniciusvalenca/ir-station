import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';


import { FirstrunPage } from '../pages/firstrun/firstrun';
import { WizardPage } from '../pages/wizard/wizard';
import { TabsPage } from '../pages/tabs/tabs';
import { DataProvider } from '../providers/data/data';


declare var WifiWizard: any;


@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  rootPage: any;
 
  row: number = 1;;
  col: number = 1;
  devicesListMap: Map<string, String[]> = new Map<string, String[]>();

  constructor(platform: Platform, statusBar: StatusBar,
    splashScreen: SplashScreen,
    public data: DataProvider
    ) {

    let self = this;
   
       

    platform.ready().then(() => {
      
     
      this.data.loadIP().then(data => {
        console.log('self.data.hostIP :'+self.data.hostIP);
        if (data == null) {
          WifiWizard.getCurrentSSID(ssidHandler, fail);
        } else {
          this.data.loadDeviceListMap().then(data=>{
            self.rootPage = TabsPage;
          })
     
                }
  
              });
  
            
     
    function ssidHandler(s) {
      console.log(s)
      if (s === '"IR-Station"') {
        console.log("igual");
        self.rootPage = WizardPage;
      }
  
      else {
        console.log("diferente");
        self.rootPage = FirstrunPage;
      }
  
    }
    function fail(e) {//declare inside the constructor locally
      alert("Por favor conecte a uma WIFI");
    }
      
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.

      statusBar.styleDefault();
      splashScreen.hide();


    });
  }




}

