import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, ToastController, LoadingController } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';
/**
 * Generated class for the AddSignalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-add-signal',
  templateUrl: 'add-signal.html',
})



export class AddSignalPage {  
    row: number;
    col: number;
    deviceRadio: string ="";
    deviceName: string ="";
    SignalName: string ="";
    


  constructor(public navCtrl: NavController, 
    public navParams: NavParams, 
    public viewCtrl: ViewController,
    public loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    public data:DataProvider) {

 
   
  }


  ionViewDidEnter() {
    console.log('ionViewDidEnter AddSignalPage');
    console.log(this.data.devicesListMap);    
    //this.devicesListMap = navParams.get('devices');
    
    this.row = this.data.row;
    this.col= this.data.col;
    this.setRowCol();
    console.log(this.row);
    console.log(this.col);
  }

  

  salvar() {
    if(this.deviceRadio != 'other'){
      console.log(this.deviceRadio+'/'+this.SignalName);    
      this.recordSignal(this.deviceRadio+'/'+this.SignalName,this.row,this.col);
    }else{
      console.log(this.deviceName+'/'+this.SignalName);
      this.recordSignal(this.deviceName+'/'+this.SignalName,this.row,this.col);    
    }
  }



  getKeys(map){
    return Array.from(map.keys());
  }

  setRowCol(){
    if(this.col+1 > 2){
      this.row++;
      this.col=1;
    }else{
      this.col++;
    }
  }


   
  cn: boolean = false;
radio_select(value) {
  if (value == 'cgt') {    
    this.cn = true;
  } else if (value == 'noncgt') {    
    this.cn = false;
  }
}



recordSignal(name, row, col) {

  let loading = this.loadingCtrl.create({
    content: 'Aguardando Sinal...'
  });  
  loading.present();

  this.data.recordSignal(name, row, col).then(data => {
    if (data) {   
       loading.dismiss();

        let toast = this.toastCtrl.create({
          message: "Sinal Gravado com sucesso!",
          duration: 1000,
          position: 'top',
          cssClass: "toast-success",
          closeButtonText :'Fechar',
          showCloseButton:false
        });
      
        toast.onDidDismiss(() => {
          //this.dismiss();
        });
      this.deviceName="";
      this.SignalName="";
      this.deviceRadio="";
      this.radio_select('noncgt');
      this.data.updateDeviceListMap();
        toast.present();

      
    }else {
      loading.dismiss();
      let toast = this.toastCtrl.create({
        message: "Problema de Comunicação",
        duration: 1000,
        position: 'top',
        cssClass: "toast-error",
        closeButtonText :'Fechar',
        showCloseButton:false
      });
    
      toast.onDidDismiss(() => {
        //console.log('Dismissed toast');
      });
    
      toast.present();
    }});

}


}
